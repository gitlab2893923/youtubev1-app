# Step 1: Build the app in a node.js environment
FROM node:14-alpine AS build

WORKDIR /app

COPY package.json ./

RUN npm install

COPY . .

RUN npm run build

# Step 2: Serve the app from Nginx
FROM nginx:alpine

COPY --from=build /app/build /usr/share/nginx/html